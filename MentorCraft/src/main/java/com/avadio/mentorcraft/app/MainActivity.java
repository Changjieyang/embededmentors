package com.avadio.mentorcraft.app;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.avadio.embeddedmentors.R;

import java.text.NumberFormat;
import java.util.Calendar;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.FROYO) {
            // Do something for froyo and above versions
            getActionBar().setDisplayHomeAsUpEnabled(true);
        } else {
            // do something for phones running an SDK before froyo
        }
        
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_club_fee) {
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /*
         *  Activity controls to handle
         */
        private EditText feeEditText;
        private EditText manualsEditText;
        private EditText initiationFeeEditText;
        private CheckBox clubInCaliforniaCheckBox;
        private Spinner joiningMonthSpinner;

        private TextView totalClubFeeTextView;
        private TextView extraInfoTextView;

        public PlaceholderFragment() {
        }

        @Override
        public void onConfigurationChanged(Configuration newConfig) {
            super.onConfigurationChanged(newConfig);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);

            joiningMonthSpinner = (Spinner) rootView.findViewById(R.id.joining_month);

            // Find current month
            int month = Calendar.getInstance().get(Calendar.MONTH);
            joiningMonthSpinner.setSelection(month);
            joiningMonthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    updateClubDue();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            // Handle text change events for the edit text controls
            feeEditText = (EditText) rootView.findViewById(R.id.fee_editText);
            feeEditText.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    updateClubDue();
                }
            });

            manualsEditText = (EditText) rootView.findViewById(R.id.manuals_editText);
            manualsEditText.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    //Toast.makeText(this, "test", Toast.LENGTH_SHORT).show();
                    updateClubDue();
                }
            });

            initiationFeeEditText = (EditText) rootView.findViewById(R.id.initiation_fee_editText);
            initiationFeeEditText.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    //Toast.makeText(this, "test", Toast.LENGTH_SHORT).show();
                    updateClubDue();
                }
            });


            clubInCaliforniaCheckBox = (CheckBox) rootView.findViewById(R.id.club_checkbox);
            clubInCaliforniaCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    updateClubDue();
                }
            });

            totalClubFeeTextView = (TextView) rootView.findViewById(R.id.total_amount_text);
            extraInfoTextView = (TextView) rootView.findViewById(R.id.extra_info);

            // Default club fee
            updateClubDue();
            return rootView;
        }

        /*
         * Compute total club fee according to fee structures.
         * As user changes form entries, club due will be automatically updated.
         */
        private void updateClubDue() {

            // EditText
            float manual;
            float feePerMonth;
            float initiationFee;

            if (manualsEditText.getText().toString().equals(""))
                manual = 0;
            else
                manual = Float.parseFloat(manualsEditText.getText().toString());

            if (feeEditText.getText().toString().equals(""))
                feePerMonth = 0;
            else
                feePerMonth = Float.valueOf(feeEditText.getText().toString());

            if (initiationFeeEditText.getText().toString().equals(""))
                initiationFee = 0;
            else
                initiationFee = Float.valueOf(initiationFeeEditText.getText().toString());

            // Get CA tax rate
            float taxRate = Constants.CA_TAX_RATE;

            //  Joining Month
            int[] monthsArray = getResources().getIntArray(R.array.months_required_array);

            // Number of months required before next renewal
            int totalMonths = monthsArray[joiningMonthSpinner.getSelectedItemPosition()];

            // Is club in California?
            boolean isCaliforniaClub = clubInCaliforniaCheckBox.isChecked();

            // For taxes
            if (isCaliforniaClub) {
                manual += manual * taxRate;
            }
            float totalFee = manual + initiationFee + (totalMonths * feePerMonth);

            NumberFormat formatter = NumberFormat.getNumberInstance();
            formatter.setMinimumFractionDigits(2);
            formatter.setMaximumFractionDigits(2);

            String output = formatter.format(totalFee);

            // Total club fee
            totalClubFeeTextView.setText(output + "");

            output = formatter.format(totalFee - manual);

            // Extra info
            extraInfoTextView.setText("(" + output + " without manuals, \n payment for " +
                    totalMonths + " months)");
        }
    }
}
