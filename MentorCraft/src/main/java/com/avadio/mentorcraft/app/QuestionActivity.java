package com.avadio.mentorcraft.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.avadio.embeddedmentors.R;
import com.avadio.mentorcraft.adapter.CustomBaseAdapter;
import com.avadio.mentorcraft.datamodel.AudioClips;
import com.avadio.mentorcraft.datamodel.Questions;
import com.avadio.mentorcraft.event.Events;
import com.avadio.mentorcraft.network.AudioBooRestClient;
import com.avadio.mentorcraft.network.HttpManager;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

public class QuestionActivity extends ActionBarActivity {
    private final String TAG = QuestionActivity.class.getSimpleName();

    private Spinner mSpinner;
    private ListView mListView;
    private List<AudioClips> mAudioClipsList = new ArrayList<AudioClips>();
    private CustomBaseAdapter mCustomBaseAdapter;
    private String[] mCategory;
    private ProgressDialog myProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        myProgressDialog = new ProgressDialog(this);
        mCategory = getResources().getStringArray(R.array.category);

        mSpinner = (Spinner) findViewById(R.id.category_spinner);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCustomBaseAdapter.resetData();
                mCustomBaseAdapter.getFilter().filter(mCategory[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Get ListView and set data adapter
        mListView = (ListView) findViewById(R.id.question_list);
        mCustomBaseAdapter = new CustomBaseAdapter(this, mAudioClipsList);
        mListView.setAdapter(mCustomBaseAdapter);

        if (!HttpManager.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(this, "Network Error.", Toast.LENGTH_SHORT).show();
            return;
        }

        // Get data
        try {
            getAudioClips();
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }

    }

    /* Subscribe to an event */
    public void onEventMainThread(Events.MediaPlayerEvents event) {

        if (event.getCommand() == Events.MediaPlayerEvents.Commands.Ready ||
                event.getCommand() == Events.MediaPlayerEvents.Commands.Complete) {
            // Change Playback background
            showDiaglog(false);
        } else {
            showDiaglog(true);
        }
        Log.d(TAG, "Event handled: " + event.getCommand());
    }

    // Talks to service to control diaglog box.
    private void showDiaglog(boolean show) {
        if (show) {
            myProgressDialog = ProgressDialog.show(this, "Please Wait", "Loading Data", true);
            myProgressDialog.setCancelable(true);
        } else {
            myProgressDialog.hide();
        }
    }

    // Fetch data
    public void getAudioClips() throws JSONException {
        AudioBooRestClient.get("", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(JSONObject obj) {
                //Toast.makeText(QuestionActivity.this, "Fetched data.", Toast.LENGTH_SHORT).show();

                // Pull out the first event on the public timeline
                try {
                    JSONObject firstEvent = obj.getJSONObject("body");
                    final String jsonString = obj.toString();

                    Gson gson = new Gson();
                    Questions questions = gson.fromJson(jsonString, Questions.class);

                    // Add to the list
                    mAudioClipsList.clear();
                    mAudioClipsList.addAll(questions.getBody().getAudio_clips());
                    mCustomBaseAdapter.notifyDataSetChanged();

                    Log.d(TAG, jsonString);
                } catch (JSONException e) {
                    Log.e(TAG, e.getMessage());
                }
            }

            @Override
            public void onFailure(Throwable e, JSONObject errorResponse) {
                Log.e("Question Activity", e.getMessage());
                Toast.makeText(QuestionActivity.this, "Error getting data, try refresh.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_club_fee) {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);

            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
        } else if (id == R.id.refresh) {
            refreshData();
        }
        return super.onOptionsItemSelected(item);
    }

    private void refreshData() {
        // Refresh data
        try {
            getAudioClips();
            mSpinner.setSelection(0);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (myProgressDialog != null)
            myProgressDialog.dismiss();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && myProgressDialog.isShowing()) {
            showDiaglog(false);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}
