package com.avadio.mentorcraft.event;

import com.avadio.mentorcraft.service.AudioPlayerService.MediaState;

/**
 * Created by ljxi_828 on 5/9/14.
 */
public class Events {
    public static class MediaPlayerEvents {
        public enum Commands{
            Play,
            Pause,
            Prepare,
            Ready,
            Complete
        }

        private Commands command;

        private String title;
        private String titleURL;

        public MediaPlayerEvents(Commands command, String title, String titleURL) {
            this.command = command;
            this.titleURL = titleURL;
        }

        public Commands getCommand() {
            return this.command;
        }
        public String getTitleURL() {
            return this.titleURL;
        }
        public String getTitle() {
            return this.title;
        }
    }

    /* */
    public static class CompletionEvent {
        public CompletionEvent(){

        }
    }
}
