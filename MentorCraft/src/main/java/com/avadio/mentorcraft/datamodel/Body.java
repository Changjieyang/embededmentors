
package com.avadio.mentorcraft.datamodel;

import java.util.List;

public class Body {
    private List<AudioClips> audio_clips;
    private Totals totals;

    public List getAudio_clips() {
        return this.audio_clips;
    }

    public void setAudio_clips(List<AudioClips> audio_clips) {
        this.audio_clips = audio_clips;
    }

    public Totals getTotals() {
        return this.totals;
    }

    public void setTotals(Totals totals) {
        this.totals = totals;
    }
}
