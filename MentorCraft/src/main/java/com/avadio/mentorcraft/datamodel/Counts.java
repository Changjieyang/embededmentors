
package com.avadio.mentorcraft.datamodel;

public class Counts{
   	private Number comments;
   	private Number plays;

 	public Number getComments(){
		return this.comments;
	}
	public void setComments(Number comments){
		this.comments = comments;
	}
 	public Number getPlays(){
		return this.plays;
	}
	public void setPlays(Number plays){
		this.plays = plays;
	}
}
