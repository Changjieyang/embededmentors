
package com.avadio.mentorcraft.datamodel;

public class Questions{
   	private Body body;
   	private Number timestamp;
   	private Number version;
   	private Number window;

 	public Body getBody(){
		return this.body;
	}
	public void setBody(Body body){
		this.body = body;
	}
 	public Number getTimestamp(){
		return this.timestamp;
	}
	public void setTimestamp(Number timestamp){
		this.timestamp = timestamp;
	}
 	public Number getVersion(){
		return this.version;
	}
	public void setVersion(Number version){
		this.version = version;
	}
 	public Number getWindow(){
		return this.window;
	}
	public void setWindow(Number window){
		this.window = window;
	}
}
