package com.avadio.mentorcraft.datamodel;

/**
 * Created by ljxi_828 on 3/28/14.
 */
public class Totals {
    private int count;
    private int offset;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
