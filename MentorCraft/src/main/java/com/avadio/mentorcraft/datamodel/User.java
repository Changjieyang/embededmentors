
package com.avadio.mentorcraft.datamodel;

public class User{
   	private Counts counts;
   	private Number id;
   	private Urls urls;
   	private String username;

 	public Counts getCounts(){
		return this.counts;
	}
	public void setCounts(Counts counts){
		this.counts = counts;
	}
 	public Number getId(){
		return this.id;
	}
	public void setId(Number id){
		this.id = id;
	}
 	public Urls getUrls(){
		return this.urls;
	}
	public void setUrls(Urls urls){
		this.urls = urls;
	}
 	public String getUsername(){
		return this.username;
	}
	public void setUsername(String username){
		this.username = username;
	}
}
