
package com.avadio.mentorcraft.datamodel;

public class Urls{
   	private String detail;
   	private String high_mp3;
   	private String wave_img;

    private String profile;
    private String image;

 	public String getDetail(){
		return this.detail;
	}
	public void setDetail(String detail){
		this.detail = detail;
	}
 	public String getHigh_mp3(){
		return this.high_mp3;
	}
	public void setHigh_mp3(String high_mp3){
		this.high_mp3 = high_mp3;
	}
 	public String getWave_img(){
		return this.wave_img;
	}
	public void setWave_img(String wave_img){
		this.wave_img = wave_img;
	}

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
