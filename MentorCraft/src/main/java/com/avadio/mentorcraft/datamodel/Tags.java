
package com.avadio.mentorcraft.datamodel;

public class Tags{
   	private String display_tag;
   	private String normalised_tag;
   	private String url;

 	public String getDisplay_tag(){
		return this.display_tag;
	}
	public void setDisplay_tag(String display_tag){
		this.display_tag = display_tag;
	}
 	public String getNormalised_tag(){
		return this.normalised_tag;
	}
	public void setNormalised_tag(String normalised_tag){
		this.normalised_tag = normalised_tag;
	}
 	public String getUrl(){
		return this.url;
	}
	public void setUrl(String url){
		this.url = url;
	}
}
