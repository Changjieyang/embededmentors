
package com.avadio.mentorcraft.datamodel;

import android.nfc.Tag;

import java.util.List;

public class AudioClips {
   	private boolean can_comment;
   	private Counts counts;
   	private String description;
   	private Number duration;
   	private Number id;
   	private String link_style;
   	private Number mp3_filesize;
   	private String recorded_at;
   	private List<Tags> tags;
   	private String title;
   	private String uploaded_at;
   	private Urls urls;
   	private User user;

 	public boolean getCan_comment(){
		return this.can_comment;
	}
	public void setCan_comment(boolean can_comment){
		this.can_comment = can_comment;
	}
 	public Counts getCounts(){
		return this.counts;
	}
	public void setCounts(Counts counts){
		this.counts = counts;
	}
 	public String getDescription(){
		return this.description;
	}
	public void setDescription(String description){
		this.description = description;
	}
 	public Number getDuration(){
		return this.duration;
	}
	public void setDuration(Number duration){
		this.duration = duration;
	}
 	public Number getId(){
		return this.id;
	}
	public void setId(Number id){
		this.id = id;
	}
 	public String getLink_style(){
		return this.link_style;
	}
	public void setLink_style(String link_style){
		this.link_style = link_style;
	}
 	public Number getMp3_filesize(){
		return this.mp3_filesize;
	}
	public void setMp3_filesize(Number mp3_filesize){
		this.mp3_filesize = mp3_filesize;
	}
 	public String getRecorded_at(){
		return this.recorded_at;
	}
	public void setRecorded_at(String recorded_at){
		this.recorded_at = recorded_at;
	}
 	public List<Tags> getTags(){
		return this.tags;
	}
	public void setTags(List<Tags> tags){
		this.tags = tags;
	}
 	public String getTitle(){
		return this.title;
	}
	public void setTitle(String title){
		this.title = title;
	}
 	public String getUploaded_at(){
		return this.uploaded_at;
	}
	public void setUploaded_at(String uploaded_at){
		this.uploaded_at = uploaded_at;
	}
 	public Urls getUrls(){
		return this.urls;
	}
	public void setUrls(Urls urls){
		this.urls = urls;
	}
 	public User getUser(){
		return this.user;
	}
	public void setUser(User user){
		this.user = user;
	}
}
