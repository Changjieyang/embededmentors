package com.avadio.mentorcraft.service;

import java.io.IOException;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.avadio.embeddedmentors.R;
import com.avadio.mentorcraft.app.QuestionActivity;
import com.avadio.mentorcraft.event.Events;

import de.greenrobot.event.EventBus;

public class AudioPlayerService extends Service implements MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener {
    private final String TAG = AudioPlayerService.class.getSimpleName();

    private static MediaState mCurrentState = MediaState.Idle;
    private static String mAudioURL;

    public static MediaState getCurrentState() {
        return mCurrentState;
    }

    /* Various states */
    public enum MediaState {
        Playing,
        Paused,
        Idle
    }

    // The action that the intent is sending
    private static final String ACTION_PLAY = "com.avadio.mentorcraft.service.action.PLAY";
    // Extra parameters
    private static final String EXTRA_PARAM1 = "com.avadio.mentorcraft.service.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "com.avadio.mentorcraft.service.extra.PARAM2";


    // Notification ID to dismiss a notification
    private final int NOTIFICATION_ID = 1;

    MediaPlayer mMediaPlayer = null;

    /**
     * Starts this service to perform action with the given parameters.
     *
     * @see Service
     */
    // TODO: Customize helper method
    public static void startAction(Context context, String param1, String param2) {
        if (mAudioURL != null && mAudioURL.equals(param1)) {
            return;
        }
        Intent intent = new Intent(context, AudioPlayerService.class);
        intent.setAction(ACTION_PLAY);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        // Every start service call will initiate this method call
        if (intent.getAction().equals(ACTION_PLAY)) {
            // Stop previous service
            stopForeground(true);

            // Start foreground service, setup notification
            startPlayback(getResources().getString(R.string.mc_name), intent.getStringExtra(EXTRA_PARAM2));

            String url = intent.getStringExtra(EXTRA_PARAM1);

            // Stop playing if url is the same as current
            if (url.equals(mAudioURL)) {
                stopPlaying();
            } else {
                stopPlaying();
                initMediaPlayer(url);
            }
        }
        return Service.START_STICKY;
    }

    private void initMediaPlayer(String url) {
        mAudioURL = url;
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        Events.MediaPlayerEvents mediaPlayerEvent = new Events.MediaPlayerEvents(Events.MediaPlayerEvents.Commands.Prepare, null, null);
        EventBus.getDefault().post(mediaPlayerEvent);

        try {
            mMediaPlayer.setDataSource(url);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
        mMediaPlayer.setOnErrorListener(this);
        mMediaPlayer.setOnPreparedListener(this);
        mMediaPlayer.setOnCompletionListener(this);
        // prepare async to not block main thread
        mMediaPlayer.prepareAsync();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        mAudioURL = null;
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        // Send broadcast to UI and dismiss the dialog
        //sendResult(Constants.PLAYBACK_PREPARED);
        // Send event
        Events.MediaPlayerEvents mediaPlayerEvent = new Events.MediaPlayerEvents(Events.MediaPlayerEvents.Commands.Ready, null, null);
        EventBus.getDefault().post(mediaPlayerEvent);
        mMediaPlayer.start();
    }

    // Start as a foreground service
    private void startPlayback(String album, String artist) {

        // Create an Intent that will open the main Activity if the notification is clicked.
        Intent intent = new Intent(this, QuestionActivity.class);
        PendingIntent pi = PendingIntent.getActivity(this, 1, intent, 0);
        // Set the Notification UI parameters
        Notification notification = new Notification(R.drawable.ic_stat_av_play, artist, System.currentTimeMillis());
        notification.setLatestEventInfo(this, album, artist, pi);
        // Set the Notification as ongoing
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        // Move the Service to the Foreground.  Stop foreground required to dismiss notification.
        startForeground(NOTIFICATION_ID, notification);
    }

    private void stopPlaying() {
        if (mMediaPlayer != null) {
            if (mMediaPlayer.isPlaying())
                mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        // Send Complete event to subscriber
        Events.MediaPlayerEvents mediaPlayerEvent = new Events.MediaPlayerEvents(Events.MediaPlayerEvents.Commands.Complete, null, null);
        EventBus.getDefault().post(mediaPlayerEvent);
    }

    /* Use Eventbus to subscribe to media player commands/events  */
    public void onEventMainThread(Events.MediaPlayerEvents event) {
        if (event.getCommand() == Events.MediaPlayerEvents.Commands.Play) {
            Log.d(TAG, "Old state is: " + getCurrentState());
            mCurrentState = MediaState.Playing;
            Log.d(TAG, "Current state is: " + getCurrentState());
        }

//        switch (event.getCommand()) {
//            case Play:
//
//                break;
//            case Pause:
//
//                break;
//            default:
//                Log.d(TAG, "Unsupported command.");
//
//        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        stopPlaying();
        stopForeground(true);
        EventBus.getDefault().unregister(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        stopPlaying();
        stopForeground(true);
        mAudioURL = null;
        mCurrentState = MediaState.Idle;
    }
}
