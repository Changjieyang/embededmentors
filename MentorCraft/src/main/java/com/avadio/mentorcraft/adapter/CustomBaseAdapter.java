package com.avadio.mentorcraft.adapter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.avadio.embeddedmentors.R;
import com.avadio.mentorcraft.app.Constants;
import com.avadio.mentorcraft.datamodel.AudioClips;
import com.avadio.mentorcraft.datamodel.Tags;
import com.avadio.mentorcraft.event.Events;
import com.avadio.mentorcraft.service.AudioPlayerService;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

import com.avadio.mentorcraft.event.Events;

/**
 * Created by ljxi_828 on 3/28/14.
 */
public class CustomBaseAdapter extends BaseAdapter implements Filterable {
    private Context mContext;
    private AudioClipsFilter mAudioClipsFilter;
    private List<AudioClips> mAudioClipsList = new ArrayList<AudioClips>();
    private List<AudioClips> mAudioClipsListOrig = new ArrayList<AudioClips>();

    public CustomBaseAdapter(Context context, List<AudioClips> audioClipsList) {
        this.mContext = context;
        this.mAudioClipsList = audioClipsList;
        this.mAudioClipsListOrig = audioClipsList;
    }

    @Override
    public int getCount() {
        return mAudioClipsList.size();
    }

    @Override
    public Object getItem(int position) {
        return mAudioClipsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder viewHolder = null;

        if (rowView == null) {
            // Only init once
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.question_list_row, null);

            viewHolder = new ViewHolder();
            viewHolder.image = (ImageView) rowView.findViewById(R.id.question_image);
            viewHolder.playbackButton = (ImageButton) rowView.findViewById(R.id.playback);
            viewHolder.title = (TextView) rowView.findViewById(R.id.title);
            viewHolder.description = (TextView) rowView.findViewById(R.id.description);
            viewHolder.length = (TextView) rowView.findViewById(R.id.length);

            rowView.setTag(viewHolder);
        }

        // Reuse convert view
        viewHolder = (ViewHolder) rowView.getTag();
        final String strInputMsgUrl = mAudioClipsList.get(position).getUrls().getHigh_mp3();
        final String strInputMsgTitle = mAudioClipsList.get(position).getTitle();

        // Load and bind to imageview
        Picasso.with(mContext).load(mAudioClipsList.get(position).getUrls().getImage()).placeholder(R.drawable.album_loading)
                .into(viewHolder.image);

        viewHolder.title.setText(mAudioClipsList.get(position).getTitle());
        viewHolder.description.setText(mAudioClipsList.get(position).getDescription());

        int secs = (int) mAudioClipsList.get(position).getDuration().floatValue();
        int remainder = secs % 3600;
        int minutes = secs / 60;
        int seconds = remainder % 60;

        String duration = "Duration: ";
        duration += minutes <= 0 ? seconds + "s" : minutes + "m" + seconds + "s";
        viewHolder.length.setText(duration);

        /* Handle play button click event */
        viewHolder.playbackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Events.MediaPlayerEvents.Commands stateTag = (Events.MediaPlayerEvents.Commands) v.getTag();
                if (stateTag == null) {
                    v.setTag(Events.MediaPlayerEvents.Commands.Play);
                    //v.setBackgroundResource(R.drawable.btn_pause);
                } else if (stateTag == Events.MediaPlayerEvents.Commands.Pause) {
                    v.setTag(Events.MediaPlayerEvents.Commands.Play);
                    //v.setBackgroundResource(R.drawable.btn_player_preview);
                } else {
                    v.setTag(Events.MediaPlayerEvents.Commands.Pause);
                    //v.setBackgroundResource(R.drawable.btn_pause);
                }

                //  Stop any running service
                mContext.stopService(new Intent(mContext, AudioPlayerService.class));
                // Call method to start service
                AudioPlayerService.startAction(mContext, strInputMsgUrl, strInputMsgTitle);
            }
        });

        return rowView;
    }

    @Override
    public android.widget.Filter getFilter() {
        if (mAudioClipsFilter == null)
            mAudioClipsFilter = new AudioClipsFilter();

        return mAudioClipsFilter;
    }

    private class AudioClipsFilter extends android.widget.Filter {
        @Override
        protected android.widget.Filter.FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
            if (constraint == null || constraint.length() == 0) {
                // No filter implemented we return all the list
                results.values = mAudioClipsList;
                results.count = mAudioClipsList.size();
            } else {
                // We perform filtering operation
                List<AudioClips> nAudioClipsList = new ArrayList<AudioClips>();

                for (AudioClips a : mAudioClipsList) {
                    // Loop through tags to get all the hashtags recorded from audioboo
                    for (Tags tag : a.getTags()) {
                        if (tag.getNormalised_tag()
                                .toUpperCase().contains(constraint.toString().toUpperCase()))
                            nAudioClipsList.add(a);
                    }
                }

                results.values = nAudioClipsList;
                results.count = nAudioClipsList.size();

            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, android.widget.Filter.FilterResults results) {
            // Now we have to inform the adapter about the new list filtered
            if (results.count == 0)
                notifyDataSetInvalidated();
            else {
                mAudioClipsList = (List<AudioClips>) results.values;
                notifyDataSetChanged();
            }
        }
    }

    // Need to restore list data back to original
    public void resetData() {
        //  Stop any running service
        //mContext.stopService(new Intent(mContext, AudioPlayerService.class));
        mAudioClipsList = mAudioClipsListOrig;
    }

    // Holder class to improve performance
    private static class ViewHolder {
        ImageView image;
        ImageButton playbackButton;
        TextView title;
        TextView description;
        TextView length;
    }
}
